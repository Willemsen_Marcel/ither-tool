# README #

### iTHER drugscreen tool ###

* Tool for calculating and visualising AUC Z-scores and some other metrics.
* Version 1.0

### How do I get set up? ###

* Create a [Bitbucket](https://bitbucket.org/product/) account and send the mail address you used to a.m.willemsen-15@prinsesmaximacentrum.nl
* Continue after you recieved a message that you have been granted access.  
* Install [git](https://git-scm.com/) and get the application code. Go to [this](https://bitbucket.org/princessmaximacenter/ither-tool/src/master/) Bitbucket repository and copy the clone command into your command line or only the URL if you use GitGUI.
* Create a [RStudio](https://rstudio.com/products/rstudio/download/) project based on the cloned directory.

### Running the tool ###

* Open the project in RStudio and sync the database by pressing `Pull Branches` in the git menu.
* Open `app.R` script in RStudio and press `Run App` in the upper right corner of the code pane. The tool will open in your favorate web browser.
* Load the input file by clicking the `Browse...` button (from iTHER directory on Shared). Or local example in project directory:

```
[project dir]/input/L10_06_iTHER_01-181_values.txt
```

* Check the fields in the popup window and choose a tumor type.
* The whole table appearing on the `Report` tab can be exported as tab delimited text file with the `Export` button.
* Open the .txt file in Excel. Select the column `AUC Z-score`, choose `Conditional Formatting -> Colour Scales -> Red - White - Green Colour Scale`.
* You can export a subset bij selecting rows in the table on the `Report` tab, clicking `Selected rows to selection` and clicking the `Export` button on the `Selection` tab.  

### Update cohort ###

* If this sample should be added to the cohort, press `Add sample to cohort`.
* Sync the database by clicking `Commit...` in the git menu in RStudio. In the popup window add a commmit message (something like: 'Added sample L10_06_iTHER_01-181 to the cohort') and click `Commit`.
* Click `Push` in the upper right corner!!!   
* Next time you use the tool the database should be synced by pressing `Pull Branches`. 
