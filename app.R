library(shiny)
library(DT)
library(shinyTree)
library(shinyjs)

source("./app_functions.R")
source("./report.functions.R")

################################################################################################################################################
# version 1.04
# library(rsconnect)
# rsconnect::deployApp()
################################################################################################################################################
ui <- fluidPage(
  useShinyjs(),
  singleton( tags$head(tags$script(src = "message-handler.js")) ),

  titlePanel("iTHER drugscreen tool (version 1.04)"),
  h3(textOutput("patient")),
  tags$br(),

  sidebarLayout(
    sidebarPanel(fluid = FALSE, width = 2,
      
      fileInput("file","Upload values:",
                multiple = FALSE,
                accept = c(".txt")
      ),
      downloadButton("get_db", "Download DB")
    ),
    mainPanel(
        tabsetPanel(id = "dataset",
                tabPanel("Report",
                         tags$br(),
                         downloadButton("export", "Export"),
                         # actionButton("select", "Selected rows to selection"),
                         tags$br(),tags$br(),
                         DTOutput("mytable2")),
                tabPanel("Compounds LP10",
                         tags$br(),
                         downloadButton("export_compounds", "Export"),
                         tags$br(),tags$br(),
                         DTOutput("mytable5")),
                tabPanel("AUC cohort",
                         tags$br(),
                         downloadButton("export_auc", "Export"),
                         tags$br(),tags$br(),
                         DTOutput("mytable3")),
                tabPanel("AUC Z-scores cohort",
                         tags$br(),
                         fluidRow(
                             column(4,
                                    downloadButton("export_z", "Export")
                             ),
                             column(4,
                                    selectInput('datasetTumorType',
                                                label = 'Tumor type',
                                                choices = c("No filter")))
                             ),
                         DTOutput("mytable6")),
                tabPanel("Metadata cohort",
                         tags$br(),
                         downloadButton("export_samples", "Export"),
                         # actionButton("toReport", "Show report"),
                         tags$br(),tags$br(),
                         DTOutput("mytable4"))
        ))))

################################################################################################################################################
################################################################################################################################################
server <- function(input, output, session) {
  
  load("./input/history/iTHER_cell_lines.RData", envir=.GlobalEnv)
  load("./input/history/iTHER_compounds.RData", envir=.GlobalEnv)
  load("./input/history/iTHER_annotation.RData", envir=.GlobalEnv)
  
  # assign("selected_rows", NULL, envir =.GlobalEnv)
  
  rv <- reactiveValues(
    report = NULL,
    samples = cell.lines,
    areas = NULL,
    drugs = compounds,
    zscores = NULL
  )

  # Set choises tumor type filter
  updateSelectInput(session, "datasetTumorType", choices = c("No filter", unique(annot$mol_diagnosis_group)))

  output$mytable5 <- DT::renderDataTable({
    DT::datatable(rv$drugs, rownames = FALSE, options = list(lengthMenu = c(250, 10, 25, 50, 100, 1000)))
  })
  output$mytable4 <- DT::renderDataTable({
    # Add the delete button column
    deleteButtonColumn(rv$samples, 'delete_button')
  })
  output$mytable3 <- DT::renderDataTable({
    df = rv$samples
    df = df[order(df[,"LP_number"], decreasing = TRUE),]
    DT::datatable(rv$areas[,c("Compound",df[,"Sample"])], rownames = TRUE, options = list(lengthMenu = c(250, 10, 25, 50, 100, 1000)))
  })
  
  updateTabsetPanel(session, "dataset", selected = "Metadata cohort")

  # Increase upload size
  options(shiny.maxRequestSize=375*1024^2)
  
  output$patient <- renderText({ 
    "Sample: "
  })
  
  dataModal <- function(failed = FALSE, cell.line, ither.number, lp.number, tumor.type, mol.diagnosis.group) {
    modalDialog(
      span(h4('Please enter fields')),
      textInput("cell_line", "Sample",
                value = cell.line
      ),
      textInput("ither_number", "iTHER #", value = ither.number),
      # selectInput("tumor_type", "Tumor type", tumor.type),
      selectInput("mol_diagnosis_group", "mol_diagnosis_group", c("",mol.diagnosis.group)),
      selectInput("mol_diagnosis_subgroup", "mol_diagnosis_subgroup", NULL),
      selectInput("molecular_diagnosis_x_specification", "molecular_diagnosis_x_specification", NULL),
      textInput("lp_number", "LP #", value = lp.number),
      checkboxInput("is_new", "This screen is part of the new (> LP5) cohort", TRUE),
      
      footer = tagList(
        modalButton("Cancel"),
        actionButton("ok", "OK")
      )
    )
  }
  
  observeEvent(input$mol_diagnosis_group, {
    index = which(annot[,1]==input$mol_diagnosis_group)
    updateSelectInput(session, "mol_diagnosis_subgroup", choices = c("",unique(annot[index,2])))
  })

  observeEvent(input$mol_diagnosis_subgroup, {
    index = which(annot[,2]==input$mol_diagnosis_subgroup)
    updateSelectInput(session, "molecular_diagnosis_x_specification", choices = c("",unique(annot[index,3])))
  })
  
  observeEvent(input$file, {
    
    # assign("selected_rows", NULL, envir =.GlobalEnv)
    load("./input/history/iTHER_annotation.RData", envir=.GlobalEnv)
    
    showModal(dataModal(cell.line = strsplit(input$file$name, "_", fixed = TRUE)[[1]][4],
                        ither.number = strsplit(input$file$name, "_", fixed = TRUE)[[1]][4],
                        tumor.type = tumorType,
                        mol.diagnosis.group = unique(annot$mol_diagnosis_group),
                        lp.number = paste(strsplit(input$file$name, "_", fixed = TRUE)[[1]][1:2], collapse = "_")))
    
    values = read.delim(input$file$datapath, sep = ",")
    rownames(values) = values$R2ID
    assign("values", values, envir =.GlobalEnv)

    output$mytable1 <- NULL
    output$mytable2 <- NULL
    
    updateTabsetPanel(session, "dataset", selected = "Report")
  })

  observeEvent(input$ok, {
    
    removeModal()
    
    # Set Sample name
    output$patient <- renderText({
      paste0("Sample: ", input$cell_line,
            "_", input$ither_number,
            "_", input$mol_diagnosis_group)
      
    })
    
    # - Load AUC
    load("./input/history/iTHER_AUC.RData", envir=.GlobalEnv)

    # - Save input file for later
    save(values, file = paste0(getwd(),"/input/history/samples/", input$cell_line, ".RData"))

    # - Add sample to cell.lines
    cell.lines = rbind(c(input$cell_line,
                         input$ither_number,
                         input$mol_diagnosis_group,
                         input$mol_diagnosis_subgroup,
                         input$molecular_diagnosis_x_specification,
                         input$lp_number,
                         input$is_new), rv$samples)
    row.names(cell.lines)[1] = input$cell_line
    rv$samples = cell.lines
    output$mytable4 <- DT::renderDataTable({
      deleteButtonColumn(rv$samples, 'delete_button')
    })

    # - Add values$AUC (from input) to AUC
    auc = cbind(rep(NA, dim(auc)[1]), auc[,c(1:dim(auc)[2])])
    colnames(auc)[1] = input$cell_line
    auc[values$R2ID, 1] = values$AUC
    rv$areas = cbind("Compound"=rv$drugs[rownames(auc), "Drug"] , auc)

    # df = rv$samples
    # df = df[order(df[,"LP_number"], decreasing = TRUE),]
    # 
    # output$mytable3 <- DT::renderDataTable({
    #   DT::datatable(rv$areas[,c("Compound",df[,"Sample"])], rownames = TRUE, options = list(lengthMenu = c(250, 10, 25, 50, 100, 1000)))
    # })

    saveCohort(rv) # First save
    updateSelectInput(session, "datasetTumorType", selected = "No filter")
    updateZScoresMatrix(input, output, rv) # Then update, this will reload AUC object

    # Display report
    cell.line = cell.lines[1,]
    
    assign("activeReport", input$cell_line, envir =.GlobalEnv)
    showReport(input, output, values, rv, cell.line)
  })

  output$export <- downloadHandler(
    filename = function() {
      "Report.txt"
    },
    content = function(file) {
      write.table(rv$report, file, sep = "\t", row.names = FALSE)
    }
  )
  
  output$export_selection <- downloadHandler(
    filename = function() {
      "Report.txt"
    },
    content = function(file) {
      write.table(new_values[selected_rows, , drop = FALSE], file, sep = "\t", row.names = FALSE)
    }
  )

  output$export_z <- downloadHandler(
    filename = function() {
      "Z-scores.txt"
    },
    content = function(file) {
      write.table(rv$zscores, file, sep = "\t", row.names = FALSE)
    }
  )

  output$export_compounds <- downloadHandler(
    filename = function() {
      "Compounds.txt"
    },
    content = function(file) {
      write.table(rv$drugs, file, sep = "\t", row.names = FALSE)
    }
  )

  output$export_auc <- downloadHandler(
    filename = function() {
      "AUC.txt"
    },
    content = function(file) {
      write.table(rv$areas, file, sep = "\t", row.names = FALSE)
    }
  )

  output$export_samples <- downloadHandler(
    filename = function() {
      "Samples.txt"
    },
    content = function(file) {
      write.table(rv$samples, file, sep = "\t", row.names = FALSE)
    }
  )
  
  observeEvent(input$mytable4_cell_edit, {
    info = input$mytable4_cell_edit
    cell.lines = rv$samples
    
    cell.lines[cell.lines[info$row, "Sample"], info$col-1] = info$value # -1 = work around because of wrong colum, very strange! 

    save(cell.lines, file = paste0(getwd(),"/input/history/iTHER_cell_lines.RData"))
    rv$samples = cell.lines
  })
  
  observeEvent(input$ok2, {
      deleteRowSamples(session, input, output, rv)
      removeModal()
  })

  observeEvent(input$toReportPressed, {
    toReport(session, input, output, values, rv)
  })
    
  observeEvent(input$deletePressed, {
    
    showModal(modalDialog(title = "Delete", "Are you sure!", footer = tagList(
      modalButton("No"),
      actionButton("ok2", "Yes")
    )))  
  })
  
  observeEvent(input$ok3, {
    # deleteRowSamples(session, input, output, rv)
    removeModal()
  })
  
  observeEvent(input$curvePressed, {
    
    rowNum = parseEvent(input$curvePressed)
    r2id = rv$report[rowNum,"R2ID"]
    experiment = experiments[[activeReport]]
    fit = experiment$fits[[r2id]]

    if (file.exists(paste0(getwd(),"/input/history/processed/", activeReport,".RData"))) {
      showModal(modalDialog(title = "Viability curve", 
                            renderPlot({
                              plot.drc(fit,main=paste0(experiment$drugs[r2id,"Drug"], "\nFit = ",
                                       round(fit$gof,2) ,"; AUC = ",round(experiment$data$auc[r2id])),
                                       crpc=log(as.numeric(experiment$drugs[names(experiment$fits)[r2id],"CRPC"]),10),plot.ic50=T)
                            }),  
      footer = tagList(
        actionButton("ok3", "Close")
      ))) 
    } else {
      showModal(modalDialog(title = "Warning", paste("No viability curves available for this screen!")))
    }
  })
  
  observeEvent(input$boxplotPressed, {
    
    rowNum = parseEvent(input$boxplotPressed)
    r2id = rownames(rv$zscores)[rowNum]
    drugType = rv$drugs[r2id, "DrugType2"]
    if (is.na(drugType)){
      drugType = rv$drugs[r2id, "DrugType1"]
      index = which(rv$drugs$DrugType1 == drugType)
      label = "(DrugType1)"
    } else {
      index = which(rv$drugs$DrugType2 == drugType)
      label = "(DrugType2)"
    }
    # showModal(modalDialog(title = "Warning", paste("length(index)", length(index))))

    if (!is.na(drugType) & length(index)>1){
    
      auc = sapply(rv$areas[rv$drugs$R2ID[index],,drop=F], as.numeric)
      # remove compound column
      auc = auc[,-1]
        
      showModal(modalDialog(title = paste("AUC boxplot", label, drugType, sep =" - "), 
                              renderPlot({
                                boxplot(auc, las = 2)
                              }),  
                              footer = tagList(
                                modalButton("Close")
                              )))
    } else {
      showModal(modalDialog(title = "Warning!", paste("No drug type classificaton available for", r2id)))      
    }
  
  })
  
  observeEvent(input$datasetTumorType, {

    if (input$datasetTumorType == "No filter") {
      samples = rownames(rv$samples)
    } else {
      index = which(rv$samples[,"mol_diagnosis_group"]==input$datasetTumorType)
      samples = NULL
      if (length(index)>0) samples = rownames(rv$samples)[index]
    }  

    if (length(samples)<2){
      updateSelectInput(session, "datasetTumorType", selected = "No filter")
      showModal(modalDialog(title = "Warning!", "Can not calculate Z-scores for less than 2 samples!"))
    } else {
      # Reload AUC data
      load("./input/history/iTHER_AUC.RData", envir=.GlobalEnv)
      auc = auc[,samples]
      rv$areas = cbind("Compound"=rv$drugs[rownames(auc), "Drug"] , auc)
      updateZScoresMatrix(input, output, rv)
    }
  })

  # tar -czvf history.tar.gz ./input/history/
  
  output$get_db <- downloadHandler(
    filename <- function(){
      search_ques_fileName = "history.tar.gz"
      return (search_ques_fileName)
    },
    content <- function(file) {
      system("tar -czvf history.tar.gz ./input/history/")
      file.copy(filename(), file)
    },
    contentType = "application/pdf"
  )
  
}

shinyApp(ui, server)